import csv
import re
import nltk, string
from sklearn.feature_extraction.text import TfidfVectorizer

nltk.download('punkt') # if necessary...


stemmer = nltk.stem.porter.PorterStemmer()
remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)

def stem_tokens(tokens):
    return [stemmer.stem(item) for item in tokens]

'''remove punctuation, lowercase, stem'''
def normalize(text):
    return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))

vectorizer = TfidfVectorizer(tokenizer=normalize, stop_words='english')

def cosine_sim(text1, text2):
    try:
        tfidf = vectorizer.fit_transform([text1, text2])
        return ((tfidf * tfidf.T).A)[0,1]
    except:
        return 0


#print(cosine_sim('a little bird', 'a little bird'))

with open('datafile.csv', newline='', encoding="ISO-8859-1") as f:
    reader = csv.reader(f)
    for row in reader:
        headline = row[1]
        tweet = row[3]
        if tweet != " not found":
            #Remove URLs
            text = re.sub(r'http\S+', '', tweet)
            text = re.sub("RT @\S+", "", text)
            text = re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", "",text)
            text = text.replace("#", "")
            text = text.replace("La Jornada: ", "")
            text = text.replace(" - La Jornada", "")
            text = text.replace("@", "")
            print(cosine_sim(headline, text))

